

var rydeAppServices = angular.module('RYDE.services', []);

rydeAppServices.factory('transportService', ['$http', function ($http) {
 var webServiceBaseUrl = "http://www.ryde.co.nz/";

    return {
        // Queue

        // api/queue/queueDriver/{id}?driverId={driverId} - POST
        queueDriver: function (id) {
            return $http.get(webServiceBaseUrl + 'api/queue/queueDriver/' + id);
        },
        // GET api/queue/queuePassenger/{passengerId}?destinationLongitude={destinationLongitude}&destinationLatitude={destinationLatitude}&destinationAddress={destinationAddress}
        queuePassenger: function (id, destinationLongitude, destinationLatitude, destinationAddress) {
            return $http.get(webServiceBaseUrl + 'api/queue/queuePassenger/' + id + '?destinationLongitude=' +
                destinationLongitude +'&destinationLatitude=' + destinationLatitude + '&destinationAddress=' + destinationAddress);
        },
        // api/queue/dequeueDriver/{id}?driverId={driverId}
        dequeueDriver: function (id) {
            return $http.get(webServiceBaseUrl + 'api/queue/dequeueDriver/' + id);
        },
        // api/queue/dequeuePassenger/{id}?passengerId={passengerId}
        dequeuePassenger: function (id) {
            return $http.get(webServiceBaseUrl + 'api/queue/dequeuePassenger/' + id);
        },

        // Journey
        // api/journey/getMatchedDriver/{id}?passengerId={passengerId}
        getMatchedDriver: function (id) {
            return $http.get(webServiceBaseUrl + 'api/journey/getMatchedDriver/' + id);
        },
        // api/journey/getMatchedPassenger/{id}?driverId={driverId}
        getMatchedPassenger: function (id) {
            return $http.get(webServiceBaseUrl + 'api/journey/getMatchedPassenger/' + id);
        },
        // api/Journey?passengerId={passengerId}&driverId={driverId}&journeyStatusId={journeyStatusId}
        journeyUpdateStatus: function (passengerId, driverId, journeyStatusId) {
            return $http.get(webServiceBaseUrl + 'api/journey/Journey?passengerId=' + passengerId + '&driverId=' + driverId + 
                '&journeyStatusId=' + journeyStatusId);
        },
        // api/Journey?passengerId={passengerId}&driverId={driverId}
        journeyGet: function (passengerId, driverId) {
            return $http.get(webServiceBaseUrl + 'api/journey/Journey?passengerId=' + passengerId + '&driverId=' + driverId );
        },

        // User
        // api/User/{id}
        user: function (id) {
            return $http.get(webServiceBaseUrl + 'api/user/' + id);
        },
        // api/User/{id}?pointBalance={pointBalance}&rating={rating}
        userPost: function (id, pointBalance, rating, kmsDriven, currentLongitude, currentLatitude) {
            return $http.get(webServiceBaseUrl + 'api/user/' + id + '?pointBalance=' + pointBalance + '&rating=' +
                rating + '&kmsDriven=' + kmsDriven + '&currentLongitude=' + currentLongitude + '&currentLatitude=' + currentLatitude);

        },

        googleTextQuery: function (searchText) {
            return $http.get("https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + searchText + "&sensor=true&key=AIzaSyBmZBA5Sy5zJs-iMAFG--IFnDeX4Eu7Rp4");
    }
        
    }
}]);


