var rydeApp = angular.module('RYDE.controllers', []);
rydeApp.controller('landingController', function ($scope, $rootScope,$location, transportService) {

    $scope.grabARyde = function () {
        transportService.user(2).then(function (result) {
            $rootScope.user = result.data;
           $location.path('findRyde').search('type', 'passenger');
        });
    };

    $scope.giveARyde = function () {
        $rootScope.user = transportService.user(1).then(function (result) {
            $rootScope.user = result.data;
            $location.path('matching').search('type', 'driver');
        });
    };
});


rydeApp.filter('unique', function () {
    return function (input, key) {
        var unique = {};
        var uniqueList = [];

        if (input) {
            for (var i = 0; i < input.length; i++) {
                if (typeof unique[input[i][key]] == "undefined") {
                    unique[input[i][key]] = "";
                    uniqueList.push(input[i]);
                }
            }
        }
        return uniqueList;
    };
});