﻿// A simple controller that fetches a list of data from a service
rydeApp.controller('tripFinishedController', function ($scope, $rootScope) {
    // "Pets" is a service returning mock data (services.js)
    $scope.distance = $rootScope.estdDist;
    $scope.KmsDriven = $rootScope.user.KmsDriven;

	$scope.sumbitGoodDriverFeedback = function() { 
    	$scope.isGoodDriver = true;
    	$scope.showGoodDriver = true;
    	$scope.showBadDriver = false;
		console.log("good driver");
     }
     
     $scope.sumbitBadDriverFeedback = function() { 
    	$scope.isGoodDriver = false;
    	$scope.showGoodDriver = false;
    	$scope.showBadDriver = true;
		console.log("bad driver");
     }
	 
    $scope.isGoodDriver = true;
    $scope.showGoodDriver = false;
    $scope.showBadDriver = false;

    
    // 
})