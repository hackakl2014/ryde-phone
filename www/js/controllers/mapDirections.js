﻿
rydeApp.controller('mapDirectionsController', function ($rootScope, $scope, $stateParams,$location, transportService) {
    console.log($stateParams);           
    $scope.location = $stateParams.location;
    $scope.isDriver = $stateParams.type == "driver";
    $scope.hasStarted = false;


    setTimeout(function () {      

        var myOptions = {
            zoom: 17,
            center: new google.maps.LatLng(40.84, 14.25),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };


        var mapObject = new google.maps.Map(document.getElementById("map"), myOptions);

        var userCircle = null;
        var matchedUserCircle = null;

        navigator.geolocation.getCurrentPosition(function (position) {
            mapObject.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
        });


        var driverId = null;
        var passengerId = null;        

        if ($scope.isDriver) {
            driverId = $rootScope.user.UserId;
            passengerId = $rootScope.matchedUser.UserId;
        }
        else{
            passengerId = $rootScope.user.UserId;
            driverId = $rootScope.matchedUser.UserId;
        }

        transportService.journeyGet(passengerId, driverId).then(function (result) {
                        
            var directionsService = new google.maps.DirectionsService();
            var directionsRequest = {
                origin: new google.maps.LatLng(result.data.OriginLatitude, result.data.OriginLongitude),
                destination: new google.maps.LatLng(result.data.DestinationLatitude, result.data.DestinationLongitude),
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC
            };
            directionsService.route(
              directionsRequest,
              function(response, status)
              {
                  if (status == google.maps.DirectionsStatus.OK)
                  {
                      new google.maps.DirectionsRenderer({
                          map: mapObject,
                          directions: response
                      });
                  }                  
              }
            );
        });

        setInterval(function () {
            //get matched User Location
            transportService.user($rootScope.matchedUser.UserId).then(function (result) {
                $rootScope.matchedUser = result.data;

                if (matchedUserCircle) {
                    matchedUserCircle.setMap(null);
                }
                
                matchedUserCircle = new google.maps.Circle({
                    map: mapObject,
                    center: new google.maps.LatLng($rootScope.matchedUser.CurrentLatitude, $rootScope.matchedUser.CurrentLongitude),
                    radius: 30,
                    strokeColor: "#f26624",
                    strokeOpacity: 0.70,
                    strokeWeight: 2,
                    fillColor: "#ca551e",
                    fillOpacity: 0.50,
                });

            });

            navigator.geolocation.getCurrentPosition(function (position) {               

                if (userCircle) {
                    userCircle.setMap(null);
                }
                
                userCircle = new google.maps.Circle({
                    map: mapObject,
                    center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    radius: 30,
                    strokeColor: "#0000ff",
                    strokeOpacity: 0.20,
                    strokeWeight: 2,
                    fillColor: "#0000ff",
                    fillOpacity: 0.050,
                });

                transportService.userPost($rootScope.user.UserId, $rootScope.user.PointBalance, $rootScope.user.Rating, $rootScope.user.KmsDriven,
                position.coords.longitude, position.coords.latitude);

            });

        }, 3000, mapObject, userCircle, matchedUserCircle);
    }, 0);

    $scope.startClick = function () {
        console.log("called");
        $scope.hasStarted = true;        
    }

    $scope.finishClick = function () {

        var driverId = null;
        var passengerId = null;

        if ($scope.isDriver) {
            driverId = $rootScope.user.UserId;
            passengerId = $rootScope.matchedUser.UserId;
        }
        else {
            passengerId = $rootScope.user.UserId;
            driverId = $rootScope.matchedUser.UserId;
        }

        //update journey status to complete
        transportService.journeyUpdateStatus(passengerId, driverId, 7);
        if ($scope.isDriver) {
            $location.path("/");

        }
        else {
            $location.path("tripFinished");
        }
    }
    
});