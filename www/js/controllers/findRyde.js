﻿
rydeApp.controller('findRydeController', function ($scope, $stateParams,transportService) {
    $scope.type = $stateParams.type;
    $scope.isDriver = $stateParams.type == "driver";
    $scope.isPassenger = $stateParams.type == "passenger";


    $scope.searchText = "";
    $scope.searchClick = function () {
        transportService.googleTextQuery($scope.searchText).then(function (result) {
            $scope.mapResults = result.data;
        }
        );
    };
})