﻿// A simple controller that fetches a list of data from a service
rydeApp.controller('matchingController', function ($rootScope, $scope, $timeout, $location, $stateParams, $window, transportService) {
    // "Pets" is a service returning mock data (services.js)
    $scope.isDriver = $stateParams.type == "driver";

    $window.navigator.geolocation.getCurrentPosition(function (position) {
            $scope.position = position;
            transportService.userPost($rootScope.user.UserId, $rootScope.user.PointBalance, $rootScope.user.Rating, $rootScope.user.KmsDriven,
        $scope.position.coords.longitude, $scope.position.coords.latitude).then(function (result) {
            if ($scope.isDriver) {
                transportService.queueDriver($rootScope.user.UserId);
            }
            else {
                transportService.queuePassenger($rootScope.user.UserId, $stateParams.targetX, $stateParams.targetY, $stateParams.targetName);
            }
        });
    }, function (error) {
        alert(error);
    });
    


    var timer = function () {
        console.log("Matching...");
        if ($scope.isDriver) {
            console.log("Matching driver...");
            transportService.getMatchedPassenger($rootScope.user.UserId).then(function (result) {
                console.log(result);

                if (result && result.data != 'null') {
                    console.log(result.data);
                    $rootScope.matchedUser = result.data;

                    clearInterval(myVar);
                    myVar = null;

                    $location.path("paired").search({
                        type: $stateParams.type, targetX: $stateParams.targetX, targetY:
                            $stateParams.targetY, targetName: $stateParams.targetName
                    });
                }
            });
        }
        else {
            console.log("Matching Passenger...");
            transportService.getMatchedDriver($rootScope.user.UserId).then(function (result) {
                console.log(result);
                if (result && result.data != 'null') {
                    console.log(result.data);
                    $rootScope.matchedUser = result.data;

                    clearInterval(myVar);
                    myVar = null;

                    $location.path("paired").search('type', $stateParams.type).search({
                        type: $stateParams.type, targetX: $stateParams.targetX, targetY:
                            $stateParams.targetY, targetName: $stateParams.targetName
                    });
                }
            });
        }
        
    };


    var myVar = setInterval(function () { timer() }, 1000);

   

    $scope.cancelClick = function ()
    {
        if ($scope.isDriver) {
            transportService.dequeueDriver($rootScope.user.UserId)
        }
        else {
            transportService.dequeuePassenger($rootScope.user.UserId)
        }
        $location.path("/");
    }
})