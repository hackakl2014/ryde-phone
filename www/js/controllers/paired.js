﻿// A simple controller that fetches a list of data from a service
rydeApp.controller('pairedController', function ($rootScope, $scope, $timeout,$location, $stateParams, transportService) {
    // "Pets" is a service returning mock data (services.js)
    $scope.type = $stateParams.type;
    $scope.isDriver = $stateParams.type == "driver";

    if (!$scope.isDriver) {
        $scope.MatchingText = 'We have found a ryde for you!';
    }
    else {
        $scope.MatchingText = 'We have found a passenger for you!';
    }


    $scope.UserId = $rootScope.matchedUser.UserId;
	$scope.KmsDriven = $rootScope.matchedUser.KmsDriven;

    console.log("Paired");
    $scope.location = $stateParams.location;

    $scope.declinelClick = function () {
        var passengerId, driverId;
        if ($scope.isDriver) {
            driverId = $rootScope.user.UserId;
            passengerId = $rootScope.matchedUser.UserId;
        }
        else {
            driverId = $rootScope.matchedUser.UserId;
            passengerId = $rootScope.user.UserId;
        }
        if ($scope.isDriver)
            transportService.journeyUpdateStatus(passengerId, driverId, 3);
        else
            transportService.journeyUpdateStatus(passengerId, driverId, 5);

        clearInterval(myVar);
        myVar = null;

        $location.path("matching").search({
            type: $stateParams.type, targetX: $stateParams.targetX, targetY:
                $stateParams.targetY, targetName: $stateParams.targetName
        });
    }
	
	/*var estdDistance = function() {
		var lat2 = $rootScope.matchedUser.CurrentLatitude; 
		var lon2 = $rootScope.matchedUser.CurrentLongitude; 
		var lat1 = $rootScope.user.CurrentLatitude; 
		var lon1 = $rootScope.user.CurrentLongitude; 

		var R = 6371; // km 
		//has a problem with the .toRad() method below.
		var x1 = lat2-lat1;
		var dLat = x1.toRad();  
		var x2 = lon2-lon1;
		var dLon = x2.toRad();  
		var a = Math.sin(dLat/2) * Math.sin(dLat/2) + 
						Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
						Math.sin(dLon/2) * Math.sin(dLon/2);  
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c;
		
	};*/
	
    Math.radians = function (degrees) {
        return degrees * Math.PI / 180;
    };

    // Converts from radians to degrees.
    Math.degrees = function (radians) {
        return radians * 180 / Math.PI;
    };

    var estdDistance = function (lat1,lat2, lon1, lon2) {

        var earthRadius = 3958.75;
        var dLat = Math.radians(lat1 - lat2);
        var dLng = Math.radians(lon1 - lon2);
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.radians(lat1)) * Math.cos(Math.radians(lat2)) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var dist = earthRadius * c;

        var meterConversion = 1609;

        return (dist * meterConversion);
	};
	
    $scope.estdDist = estdDistance($rootScope.user.CurrentLatitude, $rootScope.matchedUser.CurrentLatitude, $rootScope.user.CurrentLongitude,
         $rootScope.matchedUser.CurrentLongitude);
    $scope.estdDist = Math.round($scope.estdDist);
    $rootScope.estdDist = estdDistance($rootScope.user.CurrentLatitude, $stateParams.targetY, $rootScope.user.CurrentLongitude,
        $stateParams.targetX);
    console.log($rootScope.estdDist);
    $rootScope.estdDist = Math.round($rootScope.estdDist);

console.log($scope.estdDist );
	
    $scope.acceptClick = function () {

        var passengerId, driverId;
        if ($scope.isDriver) {
            driverId = $rootScope.user.UserId;
            passengerId = $rootScope.matchedUser.UserId;
        }
        else {
            driverId = $rootScope.matchedUser.UserId;
            passengerId = $rootScope.user.UserId;
        }
        if ($scope.isDriver)
            transportService.journeyUpdateStatus(passengerId, driverId, 2);
        else
            transportService.journeyUpdateStatus(passengerId, driverId, 4);

        $location.path("mapDirections").search({
            type: $stateParams.type
        });
    }



    //var timer = function () {
    //    console.log("pining");
    //    var passengerId, driverId;
    //    if ($scope.isDriver)
    //    {
    //        driverId = $rootScope.user.UserId;
    //        passengerId = $rootScope.matchedUser.UserId;
    //    }
    //    else
    //    {
    //        driverId = $rootScope.matchedUser.UserId;
    //        passengerId = $rootScope.user.UserId;
    //    }
    //    transportService.journeyGet(passengerId, driverId).then(function (result) {
    //            console.log(result);
    //
    //            if (result && result.data != 'null') {
    //               
    //            }
    //            else
    //            {
    //                clearInterval(myVar);
    //                myVar = null;
    //
    //                $location.path("matching").search({
    //                    type: $stateParams.type, targetX: $stateParams.targetX, targetY:
    //                        $stateParams.targetY, targetName: $stateParams.targetName
    //                });
    //            }
    //        });
    //};
    //
    //var myVar = setInterval(function () { timer() }, 1000);

});