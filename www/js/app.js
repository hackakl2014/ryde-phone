// Ionic RYDE App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'RYDE' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'RYDE.services' is found in services.js
// 'RYDE.controllers' is found in controllers.js
angular.module('RYDE', ['ionic', 'RYDE.services', 'RYDE.controllers'])


.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    console.log("win");
    $stateProvider
      .state('landing', {
          url: '/landing',
          templateUrl: 'templates/landing.html',
          controller: 'landingController'
      }
      ).state('findRyde', {
          url: '/findRyde?type',
          templateUrl: 'templates/findRyde.html',
          controller: 'findRydeController'
      }
      ).state('matching', {
          url: '/matching?type&targetName&targetX&targetY',
          templateUrl: 'templates/matching.html',
          controller: 'matchingController'
      }
      ).state('paired', {
          url: '/paired?type&targetName&targetX&targetY',
          templateUrl: 'templates/paired.html',
          controller: 'pairedController'
      }
      ).state('mapDirections', {
          url: '/mapDirections?type',
          templateUrl: 'templates/mapDirections.html',
          controller: 'mapDirectionsController'
      }
      ).state('tripFinished', {
          url: '/tripFinished',
          templateUrl: 'templates/tripFinished.html',
          controller: 'tripFinishedController'
      }
      );
    
    /*
	// the pet tab has its own child nav-view and history
    .state('tab.pet-index', {
      url: '/pets',
      views: {
        'pets-tab': {
          templateUrl: 'templates/pet-index.html',
          controller: 'PetIndexCtrl'
        }
      }
    })

    .state('tab.pet-detail', {
      url: '/pet/:petId',
      views: {
        'pets-tab': {
          templateUrl: 'templates/pet-detail.html',
          controller: 'PetDetailCtrl'
        }
      }
    })

    .state('tab.adopt', {
      url: '/adopt',
      views: {
        'adopt-tab': {
          templateUrl: 'templates/adopt.html'
        }
      }
    })

    .state('tab.about', {
      url: '/about',
      views: {
        'about-tab': {
          templateUrl: 'templates/about.html'
        }
      }
    });
	*/
  // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/landing');

});

