﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Ninject;

namespace Ryde.Server
{
    public static class NinjectHelper
    {
        public static T GetNinjectObject<T>()
        {
            IKernel _Kernal = new StandardKernel();
            _Kernal.Load(Assembly.GetExecutingAssembly());
            return _Kernal.Get<T>();
        }
    }
}