﻿using Ryde.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ryde.Server.Controllers
{
    public class UserController : BaseApiController<IUserRepository>
    {
        
        /// <summary>
        /// Get a user by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public User Get(int id)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();
            Repository.UnitOfWork = unitOfWork;

            return Repository.Where(u => u.UserId == id).FirstOrDefault();
        }

        /// <summary>
        /// Update a user's point balance, rating and geo location
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pointBalance"></param>
        /// <param name="rating"></param>
        [HttpGet]
        public void Update(int id, int pointBalance, int rating, int kmsDriven, decimal currentLongitude, decimal currentLatitude)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();
            Repository.UnitOfWork = unitOfWork;

            User user = Repository.Where(u => u.UserId == id).FirstOrDefault();

            if (user != null)
            {
                user.PointBalance = pointBalance;
                user.Rating = rating;
                user.KmsDriven = kmsDriven;
                user.CurrentLongitude = currentLongitude;
                user.CurrentLatitude = currentLatitude;

                unitOfWork.Commit();
            }
        }

    }
}
