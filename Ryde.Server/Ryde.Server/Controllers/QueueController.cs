﻿using Ryde.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ryde.Server.Controllers
{
    public class QueueController : BaseApiController<IQueueRepository>
    {

        /// <summary>
        /// Queues a User as a Driver
        /// This api will return instantaneously
        /// To check status of the match please call
        /// Api/Journal/GetMatchedDriver
        /// </summary>
        /// <param name="driverId"></param>
        [Route("api/queue/queueDriver/{driverId:int}")]
        [HttpGet]
        public void QueueDriver(int driverId)
        {
            QueueUser(driverId, (int)Enums.Role.Driver);
        }

        /// <summary>
        /// Queues a User as a Passenger
        /// This api will return instantaneously
        /// To check status of the match please call
        /// Api/Journal/GetMatchedPassenger
        /// </summary>
        /// <param name="passengerId"></param>
        /// <param name="destinationLongitude"></param>
        /// <param name="destinationLatitude"></param>
        /// <param name="destinationAddress"></param>
        [Route("api/queue/queuePassenger/{passengerId:int}")]
        [HttpGet]
        public void QueuePassenger(int passengerId, decimal destinationLongitude, decimal? destinationLatitude, string destinationAddress)
        {
            QueueUser(passengerId, (int)Enums.Role.Passenger, destinationLongitude, destinationLatitude, destinationAddress);
        }

        /// <summary>
        /// Remove a user from the queue
        /// as a driver
        /// </summary>
        /// <param name="driverId"></param>
        [Route("api/queue/dequeueDriver/{driverId:int}")]
        [HttpGet]
        public void DequeueDriver(int driverId)
        {
            DequeueUser(driverId, (int)Enums.Role.Driver);
        }

        /// <summary>
        /// Remove a user form the queue
        /// as a passenger
        /// </summary>
        /// <param name="passengerId"></param>
        [Route("api/queue/dequeuePassenger/{passengerId:int}")]
        [HttpGet]
        public void DequeuePassenger(int passengerId)
        {
            DequeueUser(passengerId, (int)Enums.Role.Passenger);
        }

        [NonAction]
        private void QueueUser(int userId, int roleId, decimal? destinationLongitude = null, decimal? destinationLatitude = null, string destinationAddress = null)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();
            Repository.UnitOfWork = unitOfWork;

            Queue queue = Repository.Where(r => r.UserId == userId &&
                r.RoleId == roleId).FirstOrDefault();

            if (queue == null)
            {
                Repository.Add(new Queue
                {
                    UserId = userId,
                    RoleId = roleId,
                    InQueueFrom = DateTime.Now,
                    DestinationLongitude = destinationLongitude,
                    DestinationLatitude = destinationLatitude,
                    DestinationAddress = destinationAddress
                });

                unitOfWork.Commit();
            }
        }

        [NonAction]
        private void DequeueUser(int userId, int roleId)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();
            Repository.UnitOfWork = unitOfWork;

            Queue queue = Repository.Where(r => r.UserId == userId && r.RoleId == roleId).FirstOrDefault();

            if (queue != null)
            {
                Repository.Delete(queue);

                unitOfWork.Commit();
            }
        }

    }
}
