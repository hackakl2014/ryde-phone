﻿using Ryde.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ryde.Server.Controllers
{
    public class JourneyController : BaseApiController<IJourneyRepository>
    {
        /// <summary>
        /// Get a Driver matched for a trip
        /// Will return null if a match has not been done
        /// </summary>
        /// <param name="passengerId"></param>
        /// <returns></returns>
        [Route("api/journey/getMatchedDriver/{passengerId:int}")]
        [HttpGet]
        public User GetMatchedDriver(int passengerId)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();                        
            Repository.UnitOfWork = unitOfWork;

           Journey journey = Repository.Where(r => r.PassengerId == passengerId && r.JourneyStatusId == (int)Enums.JourneyStatus.New).FirstOrDefault();

           if (journey != null)
           {
               return journey.Driver;
           }
           else
           {
               return null;
           }
        }

        /// <summary>
        /// Get a Passenger matched for a trip
        /// Will return null if a match has not been done
        /// </summary>
        /// <param name="driverId"></param>
        /// <returns></returns>
        [Route("api/journey/getMatchedPassenger/{driverId:int}")]
        [HttpGet]
        public User GetMatchedPassenger(int driverId)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();
            Repository.UnitOfWork = unitOfWork;

            Journey journey = Repository.Where(r => r.DriverId == driverId && r.JourneyStatusId == (int)Enums.JourneyStatus.New).FirstOrDefault();

            if (journey != null)
            {
                return journey.Passenger;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Update Status for a Journey
        /// Available Status:
        ///     1. New
        ///     2. Driver Agreed
        ///     3. Driver Declined
        ///     4. Passenger Agreed
        ///     5. Passenger Declined
        ///     6. Cancelled
        ///     7. Completed
        /// </summary>
        /// <param name="passengerId"></param>
        /// <param name="driverId"></param>
        /// <param name="journeyStatusId"></param>
        [HttpGet]
        public void Update(int passengerId, int driverId, int journeyStatusId)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();
            Repository.UnitOfWork = unitOfWork;

            Journey journey = Repository.Where(r => r.DriverId == driverId 
                && r.PassengerId == passengerId
                && r.JourneyStatusId != (int)Enums.JourneyStatus.Completed
                && r.JourneyStatusId != (int)Enums.JourneyStatus.Cancelled)
                    .FirstOrDefault();

            if (journey != null)
            {
                journey.JourneyStatusId = journeyStatusId;

                unitOfWork.Commit();
            }
        }

        /// <summary>
        /// Retrieve a Journey by passengerId and DriverId
        /// Completed Journey and Cancelled Journey will
        /// not Return
        /// </summary>
        /// <param name="passengerId"></param>
        /// <param name="driverId"></param>
        /// <returns></returns>
        [HttpGet]
        public Journey Get(int passengerId, int driverId)
        {
            EFUnitOfWork unitOfWork = new EFUnitOfWork();                        
            Repository.UnitOfWork = unitOfWork;

            Journey journey = Repository.Where(r => r.DriverId == driverId && r.JourneyStatusId != (int)Enums.JourneyStatus.Completed && r.JourneyStatusId != (int)Enums.JourneyStatus.Cancelled).FirstOrDefault();

            return journey;
        }
    }
}
