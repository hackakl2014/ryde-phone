﻿using Ryde.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Ryde.Server.Controllers
{
    public class BaseApiController<T> : ApiController
    {
        public T Repository;

        public BaseApiController()
        {
            Repository = NinjectHelper.GetNinjectObject<T>();
        }
    }
}
