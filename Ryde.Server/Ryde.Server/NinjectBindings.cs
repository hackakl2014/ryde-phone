﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ryde.DataAccess;

namespace Ryde.Server
{
    public class NinjectBindings : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IUserRepository>().To<UserRepository>();
            Bind<IQueueRepository>().To<QueueRepository>();
            Bind<IJourneyRepository>().To<JourneyRepository>();
        }
    }
}