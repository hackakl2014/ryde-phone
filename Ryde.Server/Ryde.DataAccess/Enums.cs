﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ryde.DataAccess
{
    public class Enums
    {
        public enum Role
        {
            Passenger = 1,
            Driver = 2
        }

        public enum JourneyStatus
        {
            New = 1,
            DriverAgreed = 2,
            DriverDeclined = 3,
            PassengerAgreed = 4,
            PassengerDeclined = 5,
            Cancelled = 6,
            Completed = 7
        }
    }
}
