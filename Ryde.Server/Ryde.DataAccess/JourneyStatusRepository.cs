using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Ryde.DataAccess
{   
	public  class JourneyStatusRepository : EFRepository<JourneyStatus>, IJourneyStatusRepository
	{

	}

	public  interface IJourneyStatusRepository : IRepository<JourneyStatus>
	{

	}
}