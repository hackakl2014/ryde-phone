using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Ryde.DataAccess
{   
	public  class LocationRepository : EFRepository<Location>, ILocationRepository
	{

	}

	public  interface ILocationRepository : IRepository<Location>
	{

	}
}