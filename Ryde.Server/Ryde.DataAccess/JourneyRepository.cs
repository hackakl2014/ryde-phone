using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Ryde.DataAccess
{   
	public  class JourneyRepository : EFRepository<Journey>, IJourneyRepository
	{

	}

	public  interface IJourneyRepository : IRepository<Journey>
	{

	}
}