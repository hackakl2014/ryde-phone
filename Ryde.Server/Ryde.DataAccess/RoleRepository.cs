using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Ryde.DataAccess
{   
	public  class RoleRepository : EFRepository<Role>, IRoleRepository
	{

	}

	public  interface IRoleRepository : IRepository<Role>
	{

	}
}