using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Ryde.DataAccess
{   
	public  class QueueRepository : EFRepository<Queue>, IQueueRepository
	{

	}

	public  interface IQueueRepository : IRepository<Queue>
	{

	}
}