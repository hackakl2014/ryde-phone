using System;
using System.Linq;
using System.Collections.Generic;
	
namespace Ryde.DataAccess
{   
	public  class UserRepository : EFRepository<User>, IUserRepository
	{

	}

	public  interface IUserRepository : IRepository<User>
	{

	}
}