﻿using Ryde.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ryde.JobAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            
            RydeEntities entities = new RydeEntities();
            
            while (true)
            {
                entities.Match();

                Thread.Sleep(15);
            }
        }
    }
}
